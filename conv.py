"""
Convert 10 bit h264 mkvs into 8 bit mkvs
"""

import os, sys
try:
    ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
except NameError:
    ROOT_DIR = os.path.abspath(os.path.dirname(sys.argv[0]))
import random, string
import subprocess

from MediaInfoDLL import MediaInfo, Stream
try:
    import argparse
except ImportError:
    sys.path.append(os.path.join(ROOT_DIR, 'argparse.egg'))
    import argparse


#HANDBRAKE_EXE = os.path.abspath('../handbrake/HandBrakeCLI.exe')
HANDBRAKE_EXE = os.path.abspath(os.path.join(ROOT_DIR, 'HandBrakeCLI.exe'))
X264_ENCODE_PARAMETERS = ''
HANDBRAKE_ENCODE_PARAMETERS = (
    '--encoder x264 --x264-preset veryslow --x264-tune animation '
    '--x264-profile high -q 18 --aencoder copy')
ENCODE_ALL = False

class Mediainfo(object):
    """
    Mediainfo class to get video information of a file.
    """
    def __init__(self, file_name):
        self._file_name = file_name
        self._video_bit_depth = 0
        self._audio_stream_count = self._text_stream_count = 0
        self._populate_info()

    def _populate_info(self):
        """
        Read file and store required details.
        """
        stream = MediaInfo()
        stream.Open(self._file_name)
        self._video_bit_depth = int(
            stream.Get(Stream.Video, 0, "BitDepth"))
        # Count_Get has issues
        self._audio_stream_count = int(
            stream.Get(Stream.Audio, 0, "StreamCount"))
        self._text_stream_count = int(
            stream.Get(Stream.Text, 0, "StreamCount"))
        stream.Close()

    @property
    def bit_depth(self):
        """
        Return video bit depth.
        """
        return self._video_bit_depth

    @property
    def audio_count(self):
        """
        Return audio stream count.
        """
        return self._audio_stream_count

    @property
    def subtitle_count(self):
        """
        Return subtitle stream count.
        """
        return self._text_stream_count

    @property
    def file_name(self):
        """
        Return file name of video file.
        """
        return self._file_name

def get_handbrake_a_s_parameter(mediainfo_file):
    """
    Get handbrake parameters to include all audio and subtitle tracks.
    """
    a_count = mediainfo_file.audio_count
    s_count = mediainfo_file.subtitle_count
    hb_string = ''
    if a_count > 0:
        hb_string += '-a 1'
        for i in range(2, a_count+1):
            hb_string += ','+str(i)
        hb_string += ' '
    if s_count > 0:
        hb_string += '-s 1'
        for i in range(2, s_count+1):
            hb_string += ','+str(i)
    return ' '+hb_string+' '

def generate_new_filename_from_old(old_name):
    """
    Returns a new path for the file based on the old name.
    """
    file_dir = os.path.dirname(old_name)
    old_file_name = os.path.basename(old_name)
    hi_ten_text_pos = old_file_name.lower().find('hi10p')
    if hi_ten_text_pos > -1:
        # Test_File_Hi10P_ep2.mkv -> Test_File_8-bit_ep2.mkv
        new_file_name = (old_file_name[:hi_ten_text_pos] + '8-bit' +
                         old_file_name[hi_ten_text_pos+5:])
    else:
        # Test_File_ep2.mkv -> Test_File_ep2_8-bit.mkv
        old_file_name_split = os.path.splitext(old_file_name)
        new_file_name = (old_file_name_split[0] + '_8-bit' +
                         old_file_name_split[1])
    return os.path.join(file_dir, new_file_name)

def convert_file():
    """
    Convert an individual file and remux it with the original metadata.
    """
    tmp_dir = get_tmp_path()
    raise NotImplementedError('Convert video using temporary directory ' +
                              tmp_dir)

def get_tmp_path(tmp_root=None):
    """
    Generate a temporary path/folder to demux temporary files for a video.
    """
    raise NotImplementedError('Random name generation for temporary folder '
                              'is not done yet.')
    tmp_root = tmp_root if tmp_root is not None else os.path.curdir
    tmp_dir_name = ''
    for _ in range(20):
        tmp_dir_name += random.choice(string.ascii_letters + string.digits)
    return os.path.join(tmp_root, tmp_dir_name)

def main():
    """
    Parse arguments and execute main logic.
    """
    parser = (
        argparse.ArgumentParser(description='Hi10P > Hi8P (HiP) converter.'))
    parser.add_argument('input_path', help='Input file name or path.')
    parser.add_argument('-o', '--output', help='Output file name.',
        required=False, dest='output_path')
    parser.add_argument('-t', '--tmp', help='Temporary Directory.',
        required=False, dest='tmp_path')
    parser.add_argument('-e', '--extended', help=('Extended mode. Ensure file '
        'structure compatibility.'), required=False, dest='extended',
        action='store_const', const='')
    parser.add_argument('-d', '--daemon', help='Run in daemon mode.',
        required=False, action='store_const', const='')
    parser.add_argument('--convert-all', help=('Convert 8 bit videos also '
        '(Not recommended).'), required=False, dest='con_all',
        action='store_const', const='')
    args = parser.parse_args()

    if args.extended is None:
        input_file = os.path.join(os.path.abspath('.'), args.input_path)
        output_file = (generate_new_filename_from_old(input_file) if
            args.output_path is None else args.output_path)
        vid_file = Mediainfo(input_file)
        if ENCODE_ALL or vid_file.bit_depth > 8:
            handbrake_extra_parameters = (
                get_handbrake_a_s_parameter(vid_file))
            subprocess.call(
                [HANDBRAKE_EXE,
                 '--input', input_file,
                 '--output', output_file] +
                (HANDBRAKE_ENCODE_PARAMETERS +
                 handbrake_extra_parameters).split(),
                shell=True)

    else:
        if args.daemon is None:
            if os.path.isdir(args.input_path):
                for mkv_file in os.listdir(args.input_path):
                    if mkv_file.lowercase().endswith(".mkv"):
                        convert_file()
            else:
                convert_file()

if __name__ == '__main__':
    main()
