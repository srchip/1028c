import sys

try:
    print("Trying to import built-in argparse")
    import argparse
    print("Successfully imported builtin argparse")
except ImportError:
    print("argparse not found, using egg instead")
    sys.path.append('argparse.egg')
    try:
        import argparse
        print("argparse successfully imported")
    except:
        print("argparse import error")

#try:
#    print("Trying to import installed pymediainfo")
#    from pymediainfo import MediaInfo
#    print("Successfully imported installed pymediainfo")
#except ImportError:
#    print("pymediainfo not found, using egg instead")
#    sys.path.append('pymediainfo.egg')
#    try:
#        from pymediainfo import MediaInfo
#        print("pymediainfo successfully imported")
#    except:
#        print("pymediainfo import error")

try:
    print("Trying to import MediaInfoDLL")
    import MediaInfoDLL
    print("Successfully imported MediaInfoDLL")
except ImportError:
    print("MediaInfoDLL import error")